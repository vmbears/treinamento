package br.com.skalena.treinamento.app.service;

import java.util.Collection;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaService {

	Collection<Pessoa> obterTodasPessoas();

	Pessoa obterPessoa(Integer id);

	void save(Pessoa pessoa);
	
}
