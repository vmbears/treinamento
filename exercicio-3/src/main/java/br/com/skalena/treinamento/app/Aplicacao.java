package br.com.skalena.treinamento.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.skalena.treinamento.app.config.Configuracao;
import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Aplicacao {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Configuracao.class);
		
		PessoaService service = app.getBean(PessoaService.class);
		
		log.info(service.toString());
		
		log.info("total linhas=" + service.obterTodasPessoas().size());
		
		service.save(new Pessoa(50, "exercicio 3"));
		
		log.info("total linhas=" + service.obterTodasPessoas().size());

		app.close();
	}

}