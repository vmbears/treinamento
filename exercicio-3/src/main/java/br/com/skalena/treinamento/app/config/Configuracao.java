package br.com.skalena.treinamento.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.EventListener;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan("br.com.skalena.treinamento")
public class Configuracao {


	@Autowired
	private PessoaRepository repository;
	
	@Value("skalena.treinamento.pessoa.nome-padrao")
	private String padrao;

	@EventListener(ApplicationContextEvent.class)
	public void onApplicationEvent() {
		for (int i = 0; i < 22; i++) {
			repository.save(new Pessoa(i, padrao + i));
		}
	}
	
}