package br.com.skalena.treinamento.app.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PessoaServiceImpl implements PessoaService {

	@Value("${skalena.treinamento.pessoa.nome-padrao}")
	private String nomePadrao;
	
	@Autowired
	private PessoaRepository repository;

	@Override
	public void save(Pessoa pessoa) {
		repository.save(pessoa);
	}
	
	@Override
	public Pessoa obterPessoa(Integer id) {
		log.info("obterPessoa - inicio");
		Pessoa pessoa = new Pessoa();
		pessoa.setId(id);
		pessoa.setNome(getNomePadrao());
		log.info("obterPessoa - fim");
		return pessoa;
	}

	@Override
	public Collection<Pessoa> obterTodasPessoas() {
		log.info("obterPessoa - inicio");
		List<Pessoa> pessoas = repository.findAll();
		log.info("obterPessoa - fim");
		return pessoas;
	}

	public String getNomePadrao() {
		return nomePadrao;
	}

}