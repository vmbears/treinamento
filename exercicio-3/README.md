# Assunto

Neste exercicio o objetivo � voc� ver como frameworks do Spring come�am a se comporem para montar uma aplica��o. 
Ser�o configurados o H2 Database + Hibernate + Spring Data para vermos como dados s�o acessados em um banco de dados. 


# Exercicio

Voc� deve escrever as seguintes classes:

* ConfiguracaoBancoDados.java: 
	- Nesta classe voc� deve configurar os recursos necess�rios para acessa o banco de dados.
	- Configure os recursos: banco de dados, entityManagerFactory e gestor de transa��o.
* Pessoa.java:
	- Configure as anota��es do JPA para tonar a classe uma @Entity.
* PessoaRepository.java: 
	- Nesta inteface voc� deve criar as opera��es feitas sobre a entidade de pessoa;
	- Crie um m�todo que fa�a consulta de pessoas - findByNome, selecionando registros que contiverem determinada String no atributo nome; 
* Aplicacao.java:
	- Escreva uma linha de c�digo que fa�a uma pesquisa para obter todas pessoas utilizando o m�todo findAll() e mostre o n�mero da lista;
	- Escreva uma linha de c�digo que adicione uma linha no banco, utilize o m�todo save;
	- Escreva uma linha de c�digo que fa�a uma pesquisa para obter todas pessoas utilizando o m�todo findAll() e mostre o n�mero da lista;


# Dicas

Adicionar no arquivo pom.xml

```xml
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<version>${h2.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-jpa</artifactId>
			<version>${spring.data.version}</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>${hibernate.version}</version>
		</dependency>
```

No arquivo ConfiguracaoBancoDados.java adicionar a configura��o dos seguintes objetos que ser�o gerenciados pelo Spring:

```
//Colocar essas anota��es na classe
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.skalena.treinamento.app.repository"
					 , entityManagerFactoryRef = "entityManagerFactory"
					 , transactionManagerRef = "transactionManager")
					 

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName( "org.h2.Driver");
		ds.setUrl("jdbc:h2:mem:data;DB_CLOSE_DELAY=-1");
		ds.setUsername("sa");
		ds.setPassword("password");
		return ds;
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
		emfb.setDataSource(dataSource);
		emfb.setPackagesToScan("br.com.skalena.treinamento.app.entity");
		emfb.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		Properties props = new Properties();
		props.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		props.put("hibernate.format_sql", true);
		props.put("hibernate.show_sql", false);
		props.put("hibernate.hbm2ddl.auto", "create");
		emfb.setJpaProperties(props);
		return emfb;
	}

	@Bean(name = "transactionManager")
	public JpaTransactionManager transactionmanager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
```

Na o arquivo Pessoa.java fa�a:

1. Anote a classe com @Entity;
2. Anote os atributos nome, sobrenome e dataNascimento com @Column;
3. Anote o atributo Id com: @Id e @GeneratedValue(strategy = GenerationType.AUTO);

Na interface PessoaRepository fa�a:

1. Extenda de JpaRepository<Pessoa, Integer>
2. Crie o m�todo Pessoa findByNome(String nome), para e ele seja gerado autom�ticamente em tempo de execu��o.

# Desafio:

N/A