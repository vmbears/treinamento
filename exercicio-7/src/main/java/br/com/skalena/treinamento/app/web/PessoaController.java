package br.com.skalena.treinamento.app.web;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaController {

	String obterPessoaAnterior(Integer id, HttpSession session);

	Optional<Pessoa> obterPessoa(Integer id);

	String pesquisar2(ModelMap model);

	ModelAndView pesquisar(ModelMap model);

}
