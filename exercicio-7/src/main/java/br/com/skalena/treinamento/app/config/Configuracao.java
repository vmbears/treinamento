package br.com.skalena.treinamento.app.config;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class Configuracao {

	@Autowired
	private PessoaRepository repository;
	
	@EventListener(ApplicationReadyEvent.class)
	public void registarDadosIniciais() {
		log.info("registarDadosIniciais - INICIO");
		for (int i = 1; i < 22; i++) {
			repository.save(new Pessoa(i, "exercicio " + i, "lista", LocalDate.now()));
		}
		log.info("registarDadosIniciais - FIM");
	}

}