package br.com.skalena.treinamento.app.web;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/pessoa")
@Slf4j
public class PessoaControllerImpl implements PessoaController {

	private static final String LAST_ID = "ultimo_id";

	@Autowired
	private PessoaService service;

	@Override
	@GetMapping("/pesquisar")
	public ModelAndView pesquisar(ModelMap model) {
		ModelAndView mov = new ModelAndView();
		mov.setViewName("pessoa/pesquisar");
		mov.getModel().put("msg", "Hello World !!!!");
		return mov;
	}

	@Override
	@GetMapping("/pesquisar-2")
	public String pesquisar2(ModelMap model) {
		model.put("msg", "Hello World !!!!");
		return "pessoa/pesquisar";
	}

	@Override
	@GetMapping("/{id}")
	@ResponseBody
	public Optional<Pessoa> obterPessoa(@PathVariable("id") Integer id) {
		return service.obterPessoa(id);
	}

	@GetMapping("/teste/{id}")
	@ResponseBody
	public String obterPessoaAnterior(@PathVariable("id") Integer id, HttpSession session) {
		String retorno = "id da ultima consulta = " + session.getAttribute(LAST_ID) 
					   + "\n pessoa consultada= " + service.obterPessoa(id);
		session.setAttribute(LAST_ID, id);
		return retorno;
	}

}