# Assunto

Vamos fazer nosso Hello World com ThymeLeaf !!!!  

# Exercicio

0. *Para n�o ser necess�rio a cria��o dos arquivos html, eles j� foram disponibilizados na pasta /src/main/resources/templates.* Copie os arquivos para diret�rio equivalente do seu projeto.
1. No arquivo PessoaControllerImpl:
	- Crie o m�todo pesquisar
```
	@Override
	@GetMapping("/pesquisar")
	public ModelAndView pesquisar(ModelMap model) {
		ModelAndView mov = new ModelAndView();
		mov.setViewName("pessoa/pesquisar");
		mov.getModel().put("msg", "Hello World !!!!");
		return mov;
	}
``` 
	- Crie o m�todo pesquisar2
```
		@Override
		@GetMapping("/pesquisar-2")
		public String pesquisar2(ModelMap model) {
			model.put("msg", "Hello World !!!!");
			return "pessoa/pesquisar";
		}
```
2. 
```
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-thymeleaf</artifactId>
	</dependency>
```

# Dicas

N/A

# Desafio:

N/A
