package br.com.skalena.netflix.zuul;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TesteFilter extends ZuulFilter {

	private String type;

	public TesteFilter(String type) {
		this.type = type; 
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
	    HttpServletRequest request = ctx.getRequest();
	 
	    log.info( type + " filter executado para url:	" + request.getRequestURI());
		return null;
	}

	@Override
	public String filterType() {
		return type;
	}

	@Override
	public int filterOrder() {
		return 0;
	}

}
