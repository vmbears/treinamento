package br.com.skalena.netflix.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class Zuul {
 
    public static void main(String[] args) {
        SpringApplication.run(Zuul.class, args);
    }
    
    @Bean
    public TesteFilter preFilter() {
        return new TesteFilter("pre");
    }
    
    @Bean
    public TesteFilter postFilter() {
        return new TesteFilter("post");
    }
}