# Assunto

Este projeto cria um Zuul API Gateway roteando requisições para página web e microserviços que criamos. Também foi criado um filtro simples para mostrar o conceito.

# Exercicio

1. Adicione no pom.xml o seguinte starter:

	```

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-zuul</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

	```

2. Crie o arquivo boostrap.yml com o seguinte conteúdo:

	```

		server:
		  port: 9090
		
		spring:
		  application:
		    name: zuul
		
		eureka:
		  instance:
		    hostname: localhost
		  client:
		    serviceUrl:
		      defaultZone: ${EUREKA_URI:http://localhost:8761/eureka}
		 
		zuul:
		  sensitive-headers: Cookie,Set-Cookie
		  prefix: /
		  routes:
		    pessoa:
		      path: /api/pessoa/**
		      service-id: eureka-server
		      strip-prefix: false
		    pessoa-fe:
		      path: /web/pessoa/**
		      service-id: web
		      strip-prefix: false
		      
		
		management.endpoints.web.exposure.include: hystrix.stream

	```

3. Crie a classe da inicialização da aplicação e adicione a anotação @EnableZuulProxy e @EnableDiscoveryClient

# Dicas

N/A 

# Desafio:

- Crie um filtro que é executado no inicio de todos os 