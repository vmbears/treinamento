#!/bin/bash

# apply resources part 4c: v2 to uat

# zull v2 deployment with manual sidecar injection
istioctl kube-inject –kubeconfig "~/.kube/config" \
  -f ./deployments/zull-deployment-v2-uat.yaml \
  --includeIPRanges=$IP_RANGES > \
  zull-deployment-istio.yaml \
  && kubectl apply -f zull-deployment-istio.yaml \
  && rm zull-deployment-istio.yaml
# kubectl get deployments -n uat

# route rules
kubectl apply -f ./routerules/routerule-zull-v2.yaml -n uat
# kubectl describe routerule -n uat
