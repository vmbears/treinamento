#!/bin/bash

# apply resources part 3a: just v1 to dev

# zull v1 deployment with manual sidecar injection
# kubectl apply -f ./deployments/zull-deployment-v1-dev.yaml
istioctl kube-inject –kubeconfig "~/.kube/config" \
  -f ./deployments/zull-deployment-v1-dev.yaml \
  --includeIPRanges=$IP_RANGES > \
  zull-deployment-istio.yaml \
  && kubectl apply -f zull-deployment-istio.yaml \
  && rm zull-deployment-istio.yaml

# services
kubectl apply -f ./services/zull-service.yaml -n dev

# route rules
kubectl apply -f ./routerules/routerule-zull-v1.yaml -n dev

kubectl get pods -n dev
