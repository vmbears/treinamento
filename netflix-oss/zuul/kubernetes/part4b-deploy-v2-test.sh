#!/bin/bash

# apply resources part 4b: v2 to test

# zull v2 deployment with manual sidecar injection
istioctl kube-inject –kubeconfig "~/.kube/config" \
  -f ./deployments/zull-deployment-v2-test.yaml \
  --includeIPRanges=$IP_RANGES > \
  zull-deployment-istio.yaml \
  && kubectl apply -f zull-deployment-istio.yaml \
  && rm zull-deployment-istio.yaml
# kubectl get deployments -n test

# route rules
kubectl apply -f ./routerules/routerule-zull-v2.yaml -n test
# kubectl describe routerule -n test
