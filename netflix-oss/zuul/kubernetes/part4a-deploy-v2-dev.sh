#!/bin/bash

# apply resources part 4a: v2 to dev

# zull v2 deployment with manual sidecar injection
istioctl kube-inject –kubeconfig "~/.kube/config" \
  -f ./deployments/zull-deployment-v2-dev.yaml \
  --includeIPRanges=$IP_RANGES > \
  zull-deployment-istio.yaml \
  && kubectl apply -f zull-deployment-istio.yaml \
  && rm zull-deployment-istio.yaml
# kubectl get deployments -n dev

# route rules
kubectl apply -f ./routerules/routerule-zull-v2.yaml -n dev
# kubectl describe routerule -n dev
