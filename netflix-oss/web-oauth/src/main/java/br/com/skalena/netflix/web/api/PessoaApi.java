package br.com.skalena.netflix.web.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.skalena.netflix.web.api.model.Pessoa;

@FeignClient(name = "http://eureka-server/api", fallback = PessoaFallback.class)
public interface PessoaApi {

	@RequestMapping(value = "/pessoa/", method = RequestMethod.PUT )
	Pessoa incluir(@RequestBody Pessoa pessoa);

	@RequestMapping(value = "/pessoa/", method = RequestMethod.POST )
	Pessoa alterar(@RequestBody Pessoa pessoa);

	@RequestMapping(value = "/pessoa/{id}", method = RequestMethod.DELETE )
	void excluir(@PathVariable(name = "id") Integer id) ;

	@RequestMapping(value = "/pessoa/{id}", method = RequestMethod.GET )
	Pessoa detalhar(@PathVariable(name = "id") Integer id);

	@RequestMapping(value = "/pessoa/find", method = RequestMethod.GET )
	List<Pessoa> obterTodos(@RequestParam(name = "nome") String nome);

}