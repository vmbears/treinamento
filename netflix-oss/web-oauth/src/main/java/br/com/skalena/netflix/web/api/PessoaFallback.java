package br.com.skalena.netflix.web.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.skalena.netflix.web.api.model.Pessoa;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PessoaFallback implements PessoaApi {

	@Override
	public Pessoa incluir(Pessoa pessoa) {
		log.info("incluir - " + pessoa);
		return new Pessoa();
	}

	@Override
	public Pessoa alterar(Pessoa pessoa) {
		log.info("alterar " + pessoa);
		return new Pessoa();
	}

	@Override
	public void excluir(Integer id) {
		log.info("excluir - " + id);
	}

	@Override
	public Pessoa detalhar(Integer id) {
		log.info("detalhar - " + id);
		return new Pessoa();
	}

	@Override
	public List<Pessoa> obterTodos(String nome) {
		log.info("obterTodos - " + nome);
		return new ArrayList<>();
	}

}
