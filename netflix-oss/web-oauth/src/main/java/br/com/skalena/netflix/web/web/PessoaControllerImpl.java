package br.com.skalena.netflix.web.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.skalena.netflix.web.api.PessoaApi;
import br.com.skalena.netflix.web.api.model.Pessoa;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/pessoa")
@RefreshScope
@Slf4j
public class PessoaControllerImpl implements PessoaController {

	@Autowired
	private PessoaApi service;

	@Override
	@RequestMapping("/pesquisar")
	public String pesquisar(@RequestParam(name = "search", required = false) String nome, ModelMap model) {
		SecurityContext context = SecurityContextHolder.getContext();
		log.info("context = " + context);
		if (context != null) {
			Authentication authentication = context.getAuthentication();
			log.info("authentication = " + authentication);
			if (authentication != null) {
				log.info("principal = " + authentication.getPrincipal());
			}
		}
		List<Pessoa> objetos = service.obterTodos(nome);
		model.addAttribute("pessoas", objetos);
		return "pessoa/pesquisar";
	}

	@Override
	@GetMapping("/detalhar/{id}")
	public String detalhar(@PathVariable(name = "id") Integer id, ModelMap model) {
		model.addAttribute("pessoa", service.detalhar(id));
		return "pessoa/dados";
	}

	@Override
	@GetMapping("/pre-incluir")
	public String preIncluir(ModelMap model) {
		model.addAttribute("pessoa", new Pessoa());
		model.addAttribute("acao", "incluir");
		return "pessoa/dados";
	}

	@Override
	@PostMapping("/incluir")
	public String incluir(@Valid Pessoa pessoa, ModelMap model) {
		service.incluir(pessoa);
		model.addAttribute("msg", "Sucesso");
		return pesquisar(null, model);
	}

	@Override
	@GetMapping("/pre-alterar/{id}")
	public String preAlterar(@PathVariable(name = "id") Integer id, ModelMap model) {
		model.addAttribute("pessoa", service.detalhar(id));
		model.addAttribute("acao", "alterar");
		return "pessoa/dados";
	}

	@Override
	@PostMapping("/alterar")
	public String alterar(@Valid Pessoa pessoa, ModelMap model) {
		service.alterar(pessoa);
		model.addAttribute("msg", "Sucesso");
		return pesquisar(null, model);
	}

	@Override
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable(name = "id") Integer id, ModelMap model) {
		Pessoa a = service.detalhar(id);
		if (a != null) {
			service.excluir(a.getId());
		};
		return pesquisar(null, model);
	}

	@Override
	@GetMapping("/{id}")
	@ResponseBody
	public Pessoa obter(@PathVariable(name = "id") Integer id) {
		return service.detalhar(id);
	}

}