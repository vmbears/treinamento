# Assunto

Configuração da aplicação web para requerer autenticação em um servidor OAuth2 e também repassar a mesma autenticação para os serviços acessados.   

# Exercicio

0. Copie o projeto eureka-servers
1. No pom.xml 
	- altere o artactId do projeto para eureka-server-oauth
	- adicione as seguintes dependências:
		```
			
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-security</artifactId>
			</dependency>
			<dependency>
				<groupId>org.springframework.security.oauth.boot</groupId>
				<artifactId>spring-security-oauth2-autoconfigure</artifactId>
			</dependency>

		```

2. Crie o arquivo bootstrap.yml com o seguinte conteúdo:

	```

		hystrix:
		  shareSecurityContext: true
		  command:
		    default:
		      execution:
		        isolation:
		          strategy: SEMAPHORE
		
		security:
		  oauth2:
		    client:
		      client-id: web-oauth
		      client-secret: secret
		      access-token-uri: http://localhost:8081/auth/oauth/token
		      user-authorization-uri: http://localhost:8081/auth/oauth/authorize
		    resource:
		      user-info-uri: http://localhost:8081/auth/user/me      
		  basic:
		    enabled: false
		}

	```

3. Crie o arquivo :

	```
		package br.com.skalena.netflix.web.config;
		
		import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
		import org.springframework.context.annotation.Bean;
		import org.springframework.context.annotation.Configuration;
		import org.springframework.core.annotation.Order;
		import org.springframework.security.config.annotation.web.builders.HttpSecurity;
		import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
		import org.springframework.web.context.request.RequestContextListener;
		
		import feign.RequestInterceptor;
		
		@Configuration
		@EnableOAuth2Sso
		public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
		
			@Bean
			@Order(0)
			public RequestContextListener requestContextListener() {
			    return new RequestContextListener();
			}
			
			@Bean
		    public RequestInterceptor getUserFeignClientInterceptor() {
		        return new UserFeignClientInterceptor();
		    }
		
			public void configure(HttpSecurity http) throws Exception {
				http.antMatcher("/**")
					.authorizeRequests()
					.antMatchers("/", "/login**")
					.permitAll()
					.anyRequest()
					.authenticated();
		   }
		}
	```

# Dicas

N/A 

# Desafio:

N/A 
