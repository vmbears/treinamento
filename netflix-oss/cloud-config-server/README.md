# Assunto

Spring Cloud Config é uma ferramenta para disponibilizar as propriedades para suas aplicações como serviço. Desta forma seus clientes
não armazenam informações internamente, manter as informações no ambiente. 
Mais um beneficio é que o Spring atualiza as propriedades nas classes das aplicações clientes em tempo de execução.

# Exercicio

1. Adicione no pom o seguinte starter:
```
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-server</artifactId>
		</dependency>
```
2. Crie o arquivo application.yml com o seguinte conteúdo:
```
server:
  port: 8888

spring:
  application:
    name: cloud-config-server
  cloud:
    config:
      server:
        git:
          uri: https://bitbucket.org/valdir-scarin/vmbears-properties.git
```
3. Crie a classe Application.java e adicione a anotação @EnableConfigServer

# Dicas

N/A 

# Desafio:

- Utilize a anotação @RefreshScope para atualizar o valor de propriedades utilizadas em classes do seu projeto cliente. Para realizar o teste altere valores de propriedades no seu git.