# Assunto

Utilização dos componentes do netflix oss para transformar o projeto de CRUD com Thymeleaf em uma estrutura microserviços. 

Os subprojetos deste projeto estão nomeados respectivamente com o nome do componente do netflix que utiliza.

No arquivo README.md de cada um, é mostrado como configurá-lo e no ppt do treinamento são  mostrados os conceitos utilizados.

# Exercicio

N/A

# Dicas

- Visite os sites:
	- https://12factor.net/pt_br/
	- https://eureka-servers.io
	- https://netflix.github.io/ 
	- https://projects.spring.io/spring-cloud/spring-cloud.html#_spring_cloud_netflix


# Desafio:

N/A