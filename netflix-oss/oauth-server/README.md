# Assunto

O servidor oauth2 é o responsável por indicar que as credenciais dos usuários são válidas e indicar informações do usuário autenticado.
Ele é a entidade que indica para seus componentes que uma determinada credencial está valida.
Este componente é um servidor de autenticação oauth2 com base em componentes do Spring Cloud, porém as aplicações podem usar qualquer servidor de autenticação que siga a especificação [Oauth2 | https://oauth.net/2/] 

Na brMalls deverá ser usado um componente coorporativo para essa ação, a exemplo do WSO2 Identity Server.

Desta forma este componente deve ser utilizado nos exemplos, mas não será explicado.

# Exercicio

N/A

# Dicas

N/A 

# Desafio:

N/A