# Assunto

Dashboard para apresentação dos dados coletados no hystrix das aplicações de microservicos.

# Exercicio

1. Adicione no pom o seguinte starter:
	```
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
		</dependency>
	```
2. Crie o arquivo application.yml com o seguinte conteúdo:
```
server:
  port: 8001

spring:
  application:
    name: hystrix-dashboard

eureka:
  client:
    serviceUrl:
      defaultZone: ${EUREKA_URI:http://localhost:8761/eureka}
  instance:
    preferIpAddress: true
```
3. Crie a classe da inicialização da aplicação a anotação @SpringBootApplication e @EnableHystrixDashboard

# Dicas

N/A 

# Desafio:
