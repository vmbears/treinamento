# Assunto

Este aplicativo representa os clientes de seus microserviços que precisaram acessá-los utilizando conceitos como: load-balance, service discovery e circuit-breaker. 

# Exercicio

0. Use como base o exercicio-10, removendo toda a camada de serviço e utilize como referência a interface criada no componente eureka-server
1. Adicione no pom o seguinte starter:
```
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
```
2. Crie o arquivo bootstrap.yml com o seguinte conteúdo:
```
feign:
  hystrix:
    enabled: true

eureka:
  client:
    serviceUrl:
      defaultZone: ${EUREKA_URI:http://localhost:8761/eureka}
  instance:
    preferIpAddress: true

spring:
  cloud:
    config:
      uri: http://localhost:8888
      
 
management.endpoints.web.exposure.include: hystrix.stream

```
3. Adicione na classe de inicialização do projeto as anotações @EnableDiscoveryClient, @EnableCircuitBreaker e @EnableFeignClients.
4. Crie sua classe de client para o endpoint disponibilizado no componente microservces e anote com @FeignClient(name = "http://${nome-app-registrado-no-eureka}/api", fallback = PessoaFallback.class)
5. Crie a classe que contém os métodos que serão utilizados na chamada do cliente. Ela deve implementar a interface do client de serviço e estar anotada com @Component.  

# Dicas

N/A 

# Desafio:

N/A 
