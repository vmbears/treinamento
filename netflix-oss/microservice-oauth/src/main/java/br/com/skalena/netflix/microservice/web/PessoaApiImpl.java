package br.com.skalena.netflix.eureka-server.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.skalena.netflix.eureka-server.entity.Pessoa;
import br.com.skalena.netflix.eureka-server.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pessoa")
@Slf4j
public class PessoaApiImpl implements PessoaApi {

	@Autowired
	private PessoaService service;

	@Override
	@PutMapping("/")
	public Pessoa incluir(@Valid @RequestBody Pessoa pessoa) {
		service.incluir(pessoa);
		return pessoa;
	}

	@Override
	@PostMapping("/")
	public Pessoa alterar(@Valid @RequestBody Pessoa pessoa) {
		service.alterar(pessoa);
		return pessoa;
	}

	@DeleteMapping("/{id}")
	public void excluir(@PathVariable(name = "id") Integer id) {
		service.obterPessoa(id)
			   .ifPresent(a-> service.excluir(a.getId()));
	}

	@Override
	@GetMapping("/{id}")
	public Pessoa detalhar(@PathVariable(name = "id") Integer id) {
		log.info("pesqusar");
		logSeguranca();
		return service.obterPessoa(id).get();
	}

	@RequestMapping(value = "/find", method = RequestMethod.GET )
	public List<Pessoa> obterTodos(@RequestParam(name = "nome", required = false) String nome) {
		log.info("pesqusar");
		logSeguranca();
		return service.obterPessoa(nome);
	}
	

	private void logSeguranca() {
		SecurityContext context = SecurityContextHolder.getContext();
		log.info("context = " + context);
		if (context != null) {
			Authentication authentication = context.getAuthentication();
			log.info("authentication = " + authentication);
			if (authentication != null) {
				log.info("principal = " + authentication.getPrincipal());
			}
		}
	}
}