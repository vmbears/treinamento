package br.com.skalena.netflix.eureka-server;

import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.com.skalena.netflix.eureka-server.entity.Endereco;
import br.com.skalena.netflix.eureka-server.entity.Pessoa;
import br.com.skalena.netflix.eureka-server.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaRepositories(basePackages = "br.com.skalena.netflix.eureka-server")
@Slf4j
public class eureka-server extends SpringBootServletInitializer {

	@Autowired
	private PessoaRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(eureka-server.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void registarDadosIniciais() {
		log.info("registarDadosIniciais - INICIO");
		Random rnd = new Random();
		for (int i = 1; i < 22; i++) {
			repository.save(Pessoa.builder().nome("hard code = " + i)
											.sobrenome("Teste")
											.dataNascimento(new Date())
											.endereco(new Endereco(null, "Rua Aqui", i * 100, "", getCep(rnd, i)))
											.build());
		}
		log.info("registarDadosIniciais - FIM");
	}

	private int getCep(Random rnd, int i) {
		return i * new Double(rnd.nextDouble() * 1000000d).intValue();
	}
}