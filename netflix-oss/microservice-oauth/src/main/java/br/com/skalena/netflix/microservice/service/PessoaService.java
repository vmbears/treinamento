package br.com.skalena.netflix.eureka-server.service;

import java.util.List;
import java.util.Optional;

import br.com.skalena.netflix.eureka-server.entity.Pessoa;

public interface PessoaService {

	List<Pessoa> obterTodasPessoas();

	Optional<Pessoa> obterPessoa(Integer id);

	List<Pessoa> obterPessoa(String nome);

	void excluir(Integer id);

	void alterar(Pessoa pessoa);

	void incluir(Pessoa pessoa);

}
