# Assunto

Configuração para receber autenticação e validar quando acessado via OAuth2.   

# Exercicio

0. Copie o projeto eureka-servers
1. No pom.xml 
	- altere o artactId do projeto para eureka-server-oauth
	- adicione as seguintes dependências:
		```
			
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-security</artifactId>
			</dependency>
			<dependency>
				<groupId>org.springframework.security.oauth.boot</groupId>
				<artifactId>spring-security-oauth2-autoconfigure</artifactId>
			</dependency>

		```

2. Crie o arquivo bootstrap.yml com o seguinte conteúdo:

	```

		security:
		  oauth2:
		    client:
		      client-id: web-oauth
		      client-secret: secret
		      access-token-uri: http://localhost:8081/auth/oauth/token
		      user-authorization-uri: http://localhost:8081/auth/oauth/authorize
		    resource:
		      user-info-uri: http://localhost:8081/auth/user/me      
		  basic:
		    enabled: false

	```

3. Crie a classe ResourceServerConfig.java :

	```
		
		package br.com.skalena.netflix.eureka-server;

		import org.springframework.context.annotation.Configuration;
		import org.springframework.security.config.annotation.web.builders.HttpSecurity;
		import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
		import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
		import org.springframework.security.web.util.matcher.RequestHeaderRequestMatcher;
		
		@Configuration
		@EnableResourceServer
		public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
		
		    @Override
		    public void configure(HttpSecurity http) throws Exception {
		        http.requestMatcher(new RequestHeaderRequestMatcher("Authorization"))
		            .authorizeRequests().anyRequest().fullyAuthenticated();
		    }
		}
	```

# Dicas

N/A 

# Desafio:

N/A 
