package br.com.skalena.netflix.rabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

public class MessageConfiguration {
	
	@Value("queue.name")
	private String queueName;
	
	@Bean
	public Queue queue() {
	    return new Queue(queueName, false);
	}
	
}