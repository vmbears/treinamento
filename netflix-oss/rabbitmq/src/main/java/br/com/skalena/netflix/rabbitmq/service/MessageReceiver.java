package br.com.skalena.netflix.rabbitmq.service;

import java.util.concurrent.CountDownLatch;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.skalena.netflix.rabbitmq.model.Pessoa;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MessageReceiver {

	private CountDownLatch latch = new CountDownLatch(1);

	@RabbitListener(queues = {"${queue.name}"})
    public void receiveMessage(@Payload Pessoa pessoa) {
        log.info("Mensagem recebida >> " + pessoa );
    }

}