package br.com.skalena.netflix.rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableRabbit
@SpringBootApplication
public class RabbitMQ {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(RabbitMQ.class, args);
    }

}