package br.com.skalena.netflix.rabbitmq.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pessoa implements Serializable {

	private Integer id;
	
	private String nome;
	
}
