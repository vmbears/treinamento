package br.com.skalena.netflix.rabbitmq.service;

import java.util.Random;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.skalena.netflix.rabbitmq.model.Pessoa;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MessageSender {

	@Autowired
    private RabbitTemplate rabbitTemplate;

	@Value("${queue.name}")
	private String queueName;

//    @Scheduled(fixedDelay = 3000)
    public void run() throws Exception {
        log.info("Sending message...");
        rabbitTemplate.convertAndSend(queueName, new Pessoa(new Random().nextInt(), "Teste Mensagem"));
    }

}