# Assunto

O serviço de registro e descoberta deve ser publicado para que os clientes possam compartilhar suas instâncias. O Eureka revolve esse problema tanto no cliente como no servidor.
A seguir estão descritos os passos para configurar um servidor Eureka. 

# Exercicio

1. Adicione no pom o seguinte starter:
```
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
		</dependency>
```
2. Crie o arquivo application.yml com o seguinte conteúdo:
```
spring:
  application:
    name: eureka-server

eureka:
  instance:
    hostname: no-1
    instanceId: instancia-1
  client:
    register-with-eureka: false
    fetch-registry: false

server:
  port: 8761
```
3. Crie a classe da inicialização da aplicação e adicione a anotação @EnableEurekaServer

# Dicas

N/A 

# Desafio:

N/A