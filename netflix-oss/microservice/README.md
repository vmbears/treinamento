# Assunto

Este aplicativo representa um microserviço que expoe endpoints para operar funções sobre dados. Ele se registra no eureka e obtem propriedades do config-server 

# Exercicio

0. Use como base o exercicio-10, removendo toda a camada web e exponha uma API para operações sobre as entidades

1. No pom.xml 
	- altere o pom-parent para:
		```
			<parent>
				<groupId>br.com.skalena.netflix</groupId>
				<artifactId>netflix-oss</artifactId>
				<version>0.0.1-SNAPSHOT</version>
			</parent>
		```
	- adicione as dependências:
		```
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-starter-config</artifactId>
			</dependency>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
			</dependency>
		```

2. Crie o arquivo application.yml com o seguinte conteúdo:
		```
			eureka:
			  client:
			    serviceUrl:
			      defaultZone: ${EUREKA_URI:http://localhost:8761/eureka}
			  instance:
			    preferIpAddress: true
			
			spring:
			  application:
			    name: eureka-server
			  cloud:
			    config:
			      uri: http://localhost:8888
		```
3. Adicione na classe  de inicialização do projeto a anotação @EnableDiscoveryClient

4. Adicione o método na classe de inicialização para carregar os dados inicias :

```
	
	@Autowired
	private PessoaRepository repository;
	
	@Value("${vmbears.eureka-server.default-pessoa-name}")
	private String nomePadraoPessoa;
	
	private int getCep(Random rnd, int i) {
		return i * new Double(rnd.nextDouble() * 1000000d).intValue();
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void registarDadosIniciais() {
		log.info("registarDadosIniciais - INICIO");
		Random rnd = new Random();
		for (int i = 1; i < 22; i++) {
			repository.save(Pessoa.builder().nome(nomePadraoPessoa + i)
											.sobrenome("Teste")
											.dataNascimento(new Date())
											.endereco(new Endereco(null, "Rua Aqui", i * 100, "", getCep(rnd, i)))
											.build());
		}
		log.info("registarDadosIniciais - FIM");
	}
```

4. Crie um arquivo chamado PessoaApiImpl:
	```
		package br.com.skalena.netflix.microservice.web;
		
		import java.util.List;
		
		import javax.validation.Valid;
		
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.web.bind.annotation.DeleteMapping;
		import org.springframework.web.bind.annotation.GetMapping;
		import org.springframework.web.bind.annotation.PathVariable;
		import org.springframework.web.bind.annotation.PostMapping;
		import org.springframework.web.bind.annotation.PutMapping;
		import org.springframework.web.bind.annotation.RequestBody;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestMethod;
		import org.springframework.web.bind.annotation.RequestParam;
		import org.springframework.web.bind.annotation.RestController;
		
		import br.com.skalena.netflix.microservice.entity.Pessoa;
		import br.com.skalena.netflix.microservice.service.PessoaService;
		import lombok.extern.slf4j.Slf4j;
		
		@RestController
		@RequestMapping("/pessoa")
		@Slf4j
		public class PessoaApiImpl implements PessoaApi {
		
			@Autowired
			private PessoaService service;
		
			@Override
			@PutMapping("/")
			public Pessoa incluir(@Valid @RequestBody Pessoa pessoa) {
				service.incluir(pessoa);
				return pessoa;
			}
		
			@Override
			@PostMapping("/")
			public Pessoa alterar(@Valid @RequestBody Pessoa pessoa) {
				service.alterar(pessoa);
				return pessoa;
			}
		
			@DeleteMapping("/{id}")
			public void excluir(@PathVariable(name = "id") Integer id) {
				service.obterPessoa(id)
					   .ifPresent(a-> service.excluir(a.getId()));
			}
		
			@Override
			@GetMapping("/{id}")
			public Pessoa detalhar(@PathVariable(name = "id") Integer id) {
				return service.obterPessoa(id).get();
			}
		
			@RequestMapping(value = "/find", method = RequestMethod.GET )
			public List<Pessoa> obterTodos(@RequestParam(name = "nome", required = false) String nome) {
				log.info("pesqusar");
				return service.obterPessoa(nome);
			}
		}
	```

5. Criar uma classe chamada PessoaApi.java:

	```
		package br.com.skalena.netflix.microservice.web;
		
		import java.util.List;
		
		import org.springframework.web.bind.annotation.PathVariable;
		import org.springframework.web.bind.annotation.RequestBody;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestMethod;
		import org.springframework.web.bind.annotation.RequestParam;
		
		import br.com.skalena.netflix.microservice.entity.Pessoa;
		
		public interface PessoaApi {
		
			@RequestMapping(value = "/pessoa/", method = RequestMethod.PUT )
			Pessoa incluir(@RequestBody Pessoa pessoa);
		
			@RequestMapping(value = "/pessoa/", method = RequestMethod.POST )
			Pessoa alterar(@RequestBody Pessoa pessoa);
		
			@RequestMapping(value = "/pessoa/{id}", method = RequestMethod.DELETE )
			void excluir(@PathVariable(name = "id") Integer id) ;
		
			@RequestMapping(value = "/pessoa/{id}", method = RequestMethod.GET )
			Pessoa detalhar(@PathVariable(name = "id") Integer id);
		
			@RequestMapping(value = "/pessoa/find", method = RequestMethod.GET )
			List<Pessoa> obterTodos(@RequestParam(name = "nome") String nome);
		
		}
	```

# Dicas

N/A 

# Desafio:

N/A 
