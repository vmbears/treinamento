package br.com.skalena.netflix.microservice;

import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.com.skalena.netflix.microservice.entity.Endereco;
import br.com.skalena.netflix.microservice.entity.Pessoa;
import br.com.skalena.netflix.microservice.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "br.com.skalena.netflix.microservice")
@EnableDiscoveryClient
@Slf4j
public class Microservice extends SpringBootServletInitializer {

	@Autowired
	private PessoaRepository repository;
	
	@Value("${vmbears.microservico.default-pessoa-name}")
	private String nomePadraoPessoa;

	public static void main(String[] args) {
		SpringApplication.run(Microservice.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void registarDadosIniciais() {
		log.info("registarDadosIniciais - INICIO");
		Random rnd = new Random();
		for (int i = 1; i < 22; i++) {
		 	repository.save(Pessoa.builder().nome(nomePadraoPessoa + i)
											.sobrenome("Teste")
											.dataNascimento(new Date())
											.endereco(new Endereco(null, "Rua Aqui", i * 100, "", getCep(rnd, i)))
											.build());
		}
		log.info("registarDadosIniciais - FIM");
	}

	private int getCep(Random rnd, int i) {
		return i * new Double(rnd.nextDouble() * 1000000d).intValue();
	}
}