package br.com.skalena.netflix.microservice.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.skalena.netflix.microservice.entity.Pessoa;
import br.com.skalena.netflix.microservice.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	private PessoaRepository repository;

	@Override
	public Optional<Pessoa> obterPessoa(Integer id) {
		return repository.findById(id);
	}
	
	@Override
	public void excluir(Integer id) {
		repository.deleteById(id);
	}
	
	@Override
	public void incluir(Pessoa pessoa) {
		repository.save(pessoa);
	}
	
	@Override
	public void alterar(Pessoa pessoa) {
		repository.save(pessoa);
	}
	
	@Override
	public List<Pessoa> obterPessoa(String nome) {
		List<Pessoa> pessoas;
		if (nome != null) {
			pessoas = repository.findByNomeContaining(nome);
		} else {
			pessoas = repository.findAll();
		}
		return pessoas;
	}

	@Override
	public List<Pessoa> obterTodasPessoas() {
		return repository.findAll();
	}

}