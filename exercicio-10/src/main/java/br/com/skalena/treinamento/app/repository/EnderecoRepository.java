package br.com.skalena.treinamento.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.skalena.treinamento.app.entity.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
