# Assunto

Ativação do Spring Data Rest para obter acesso ao PessoaRepository por meio de URL que utilizando o protocolo HAL.


# Exercicio

1. Adicionar configuração do spring data rest no pom.xml:

```
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-data-rest</artifactId>
	</dependency>
```

2. Adicionar a declaração do context-path do spring data rest no application.yml:

```
spring:
  data:
    rest:
      basePath: /api
```
3. Acesse http://localhost:8080/api/
4. Acesse http://localhost:8080/api/pessoas?page=1&size=1&sort=nome,asc

# Dicas

- https://docs.spring.io/spring-data/rest/docs/3.1.8.RELEASE/reference/html/#getting-started.changing-other-properties
- https://docs.spring.io/spring-data/rest/docs/3.1.8.RELEASE/reference/html/#repository-resources.methods
- https://docs.spring.io/spring-data/rest/docs/3.1.8.RELEASE/reference/html/#projections-excerpts
- https://docs.spring.io/spring-data/rest/docs/3.1.8.RELEASE/reference/html/#validation
- https://docs.spring.io/spring-data/rest/docs/3.1.8.RELEASE/reference/html/#events

# Desafio

N/A