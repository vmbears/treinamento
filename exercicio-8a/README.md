# Assunto

Para exemplificar como � feito o uso do Spring Boot + Spring MVC + ThymeLeaf, o objetivo deste exerc�cio � fazer um CRUD simmples explorando as caracteristicas de transforma��o de templates.  

# Exercicio

0. *Para n�o ser necess�rio a cria��o dos arquivos html, eles j� foram disponibilizados na pasta /src/main/resources/templates.* Copie os arquivos para diret�rio equivalente do seu projeto. Ser� necess�rio apenas a adic�o das nota��es do ThymeLeaf;
1. Copie a src\main\java\br\com\skalena\treinamento\app\web\PessoaControllerImpl.java para seu projeto em pasta equivalente;
2. Na classe PessoaControllerImpl.java preencha o comportamento dos m�todos  de crud chamando o respectivo comportamento do objeto PessoaService;
3.  
# Dicas

# Exerc�cio

# Desafio