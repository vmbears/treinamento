package br.com.skalena.treinamento.app.config;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class Configuracao extends WebMvcConfigurationSupport {

	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) { 
    	registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

	@Autowired
	private PessoaRepository repository;

	@EventListener(ApplicationReadyEvent.class)
	public void registarDadosIniciais() {
		log.info("registarDadosIniciais - INICIO");
		for (int i = 1; i < 22; i++) {
			repository.save(new Pessoa(i, "exercicio " + i, "lista", new Date()));
		}
		log.info("registarDadosIniciais - FIM");
	}

}