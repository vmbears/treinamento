package br.com.skalena.treinamento.app.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaService {

	Collection<Pessoa> obterTodasPessoas();

	Optional<Pessoa> obterPessoa(Integer id);

	List<Pessoa> obterPessoa(String nome);

	void excluir(Integer id);

	void alterar(Pessoa pessoa);

	void incluir(Pessoa pessoa);

}
