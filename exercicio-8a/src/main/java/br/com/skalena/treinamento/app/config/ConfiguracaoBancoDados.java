package br.com.skalena.treinamento.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.skalena.treinamento.app.repository")
public class ConfiguracaoBancoDados {

}