package br.com.skalena.treinamento.app.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@PropertySource("classpath:application.properties")
@ComponentScan("br.com.skalena.treinamento")
@EnableAsync
@EnableScheduling
public class Configuracao {

}