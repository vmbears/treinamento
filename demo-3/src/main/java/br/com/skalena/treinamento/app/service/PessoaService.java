package br.com.skalena.treinamento.app.service;

import java.util.Collection;
import java.util.concurrent.Future;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaService {

	Collection<Pessoa> obterTodasPessoas();

	Pessoa obterPessoa(Integer id);

	Future<Pessoa> obterPessoaDemorada(Integer id);
	
}
