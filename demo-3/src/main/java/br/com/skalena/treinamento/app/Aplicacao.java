package br.com.skalena.treinamento.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.skalena.treinamento.app.config.Configuracao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Aplicacao {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Configuracao.class);
		
	}

}