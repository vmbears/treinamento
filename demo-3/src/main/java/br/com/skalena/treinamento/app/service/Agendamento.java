package br.com.skalena.treinamento.app.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Agendamento {
	
	@Scheduled(cron = "*/1 * * * * *")
	public void processarPessoa() {
		log.info("teste");
	}
	
	@Scheduled(fixedDelay = 4000)
	public void job2() {
		log.info("4 segundos - silvana");
		log.info("4 segundos - valdir");
		log.info("4 segundos - teste valdir");
	}
	
}

