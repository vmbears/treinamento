package br.com.skalena.treinamento.app.config;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestInterceptor implements MethodInterceptor {

	private Object result;
	
	public TestInterceptor(Object result) {
		this.result = result;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		log.info("teste do valdir" + invocation.getArguments());
		return invocation.getMethod().invoke(result, invocation.getArguments());
	}
}