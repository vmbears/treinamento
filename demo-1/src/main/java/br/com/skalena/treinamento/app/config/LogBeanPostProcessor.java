package br.com.skalena.treinamento.app.config;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.stereotype.Component;

import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LogBeanPostProcessor implements MergedBeanDefinitionPostProcessor {
	
	@Override
	public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
		log.info("1 - postProcessMergedBeanDefinition - {} - {}", beanType, beanName);
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		log.info("2 - postProcessBeforeInitialization - {}", beanName);
		log.info("instancia do objeto = {}", bean);

		return MergedBeanDefinitionPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
	}
	
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		log.info("3 - postProcessAfterInitialization - {}", beanName);
		log.info("instancia do objeto = {}", bean);
		Object result = MergedBeanDefinitionPostProcessor.super.postProcessAfterInitialization(bean, beanName);
		if (bean instanceof PessoaService) {
			result = ProxyFactory.getProxy(PessoaService.class, new TestInterceptor(result));
		}
		return result;
	}
	
}