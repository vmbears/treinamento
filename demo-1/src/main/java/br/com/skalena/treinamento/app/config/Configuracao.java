package br.com.skalena.treinamento.app.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application.properties")
@ComponentScan("br.com.skalena.treinamento")
@EnableAspectJAutoProxy
public class Configuracao {

}