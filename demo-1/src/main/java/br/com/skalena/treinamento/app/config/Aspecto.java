package br.com.skalena.treinamento.app.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Slf4j
public class Aspecto {
	
    @Around("execution(* br.com.skalena.*.*(..))")
    public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
		log.info("inicio exec");
		Object retVal = pjp.proceed();
		log.info("fim exec");
        return retVal;
    }
}