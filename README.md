# Treinamento VMBears & Skalena

Os exerc�cios s�o organizados para seguir o conte�do que � apresentado no no treinamento.

O projeto exerc�cio-10 � o inicio que possui as configura��es b�sicas para execu��o dos pr�ximos exerc�cios.

Os exerc�cios dependem entre si, desta forma deve-se finalizar o exerc�cio para come�ar o subsequente. 
A ordem dos exerc�cios � dada pelo pr�prio nome do exerc�cio.

Dentro da pasta de cada exerc�cio, no arquivo README.md tem a descri��o do exerc�cio separados nas sess�es: assunto, dicas, problema e desaf�o.

A sess�o dica � bastante importante, por que nela s�o apresentados trechos de c�digo que devem ser utilizados para configura��o do exercicio anterior, tornando ele aderente a necessidade do exerc�cio atual.    

Caso n�o seja poss�vel terminar o exerc�cio iniciado, na pr�pria pasta do exercicio est�o os arquivos com a resolu��o do mesmo.