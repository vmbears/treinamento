package br.com.skalena.treinamento.app.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

	Pessoa findByNome(String nome);
	
	Page<Pessoa> findByNome(String nome, Pageable pageable);
	
	Pessoa findByNomeAndSobrenome(String nome, String sobrenome);

	@Query("SELECT p FROM Pessoa p WHERE 0 <> (p.id % 2) ")
	List<Pessoa> findIdImpar();

	@Query("SELECT p FROM Pessoa p WHERE 0 <> (p.id % 2) ")
	Page<Pessoa> findIdImpar(Pageable pageable);
	
	
	@Query("SELECT SUM(p.id) FROM Pessoa p")
	Integer consultarSomaId();

}
