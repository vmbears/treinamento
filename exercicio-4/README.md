# Assunto

No Spring Data as queries s�o geradas a partir do nome dos m�todos, mas tamb�m � poss�vel criar queries especificas.
Neste exercicio ser�o criadas consultas dos dois tipos para o reposit�rio PessoaRepository.  

# Exercicio

Criar queries do tipo:

1. Autom�tica
	- Com dois parametros de consulta e sendo gerada automaticamente
	- Com dois parametros de consulta e sendo gerada automaticamente e utilizando pagina��o
2. Manual
	- Uma consulta que retorne todas as pessoas com o ID impar
	- Uma consulta que retorne todas as pessoas com o ID impar com pagina��o
	- Uma soma de todos os ids - SUM;

# Dicas

- Para indicar parametros de pagina��o em consultas manuais ou automaticas, utilize o parametro Pageable na sua consulta. Quando fizer isso o retorno da sua classe deve ser a interface Page<T>
- Utilize a anota��o @Query para declarar a sua consulta manualmente;
- A JPQL para consultar n�meros impares � : SELECT p FROM Pessoa p WHERE 0 <> (p.id % 2);
- Para construir um objeto Pageable utilize a classe PageRequest.

# Desafio:

- Faz consultas com os tipos operadores como LessThan, Contains, NotContains e In. Veja a apresenta��o para maiores informa��es.