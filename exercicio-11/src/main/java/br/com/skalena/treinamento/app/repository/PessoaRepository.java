package br.com.skalena.treinamento.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

	List<Pessoa> findByNomeContaining(String nome);

}
