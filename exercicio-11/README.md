# Assunto

Utilizar o plugin do maven para gerar imagens docker.

# Exercicio	

1. Adicione no arquivo pom.xml o plugin dentro da sessão <build>/<plugins>:
```
	<plugin>
		<groupId>com.spotify</groupId>
		<artifactId>docker-maven-plugin</artifactId>
		<version>1.2.0</version>
		<configuration>
			<imageName>${project.artifactId}</imageName>
			<baseImage>java</baseImage>
			<entryPoint>["java", "-jar",  "/${project.build.finalName}.jar"]</entryPoint>
			<resources>
				<resource>
					<targetPath>/</targetPath>
					<directory>${project.build.directory}</directory>
					<include>${project.build.finalName}.jar</include>
				</resource>
			</resources>
		</configuration>
	</plugin>
```
2. Execute na linha de comando, na pasta do projeto: mvn clean install docker:build
3. Veja no repositorio local a imagem que foi gerada: docker images
4. Execute a imagem com o docker: docker run -it -d -p 8080:8080 exercicio-11

# Dicas

N/A

# Desafio

N/A