package br.com.skalena.treinamento.app.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import br.com.skalena.treinamento.app.service.PessoaService;
import br.com.skalena.treinamento.app.service.PessoaServiceImpl;
import br.com.skalena.treinamento.app.service.PessoasExternoServiceImpl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfiguracaoManual {
	
	@Bean
	public static PropertyPlaceholderConfigurer properties() {
	    PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
	    Resource[] resources = new ClassPathResource[] { new ClassPathResource( "application.properties" ) };
	    ppc.setLocations( resources );
	    ppc.setIgnoreUnresolvablePlaceholders( true );
	    return ppc;
	}
	
	@Bean
	@Primary
	public PessoaService internoBean() {
		return new PessoaServiceImpl();
	}
	
	@Bean
	public PessoaService externoBean() {
		return new PessoasExternoServiceImpl();
	}

}