package br.com.skalena.treinamento.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@PropertySource("classpath:application.properties")
@ComponentScan("br.com.skalena.treinamento")
@EnableAsync
public class Configuracao {

	@Bean
	public TaskExecutor taskexecutor() {
		ThreadPoolTaskExecutor task = new ThreadPoolTaskExecutor();
		task.setCorePoolSize(10);
		task.setMaxPoolSize(50);
		task.setQueueCapacity(20000);
		task.setDaemon(true);
		return task;
	}
	
}