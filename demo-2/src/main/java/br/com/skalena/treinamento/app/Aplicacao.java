package br.com.skalena.treinamento.app;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import br.com.skalena.treinamento.app.config.Configuracao;
import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Aplicacao {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Configuracao.class);
		
		PessoaService negocio = app.getBean(PessoaService.class);
		
		List<Future<Pessoa>> lista = new ArrayList<>();
		for (int i = 1; i < 20000; i++) {
			lista.add(negocio.obterPessoaDemorada(i));
		}
		
		for (Future<Pessoa> item : lista) {
			log.info(item.get().toString());
		}

		app.close();
	}

}