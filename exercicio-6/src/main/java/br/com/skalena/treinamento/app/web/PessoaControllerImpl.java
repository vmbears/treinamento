package br.com.skalena.treinamento.app.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/pessoa")
@Slf4j
public class PessoaControllerImpl {

	private static final int PAGE_SIZE_DEFAULT = 5;
	private static final int PAGE_NUMBER_DEFAULT = 0;

	@Autowired
	private PessoaService service;

	@RequestMapping("/pesquisar")
	public String pesquisar(@RequestParam(name = "search", required = false) String nome
						  , @RequestParam(name = "page", defaultValue = "0", required = false) Integer page
						  , @RequestParam(name = "size", defaultValue = "5", required = false) Integer size
						  , ModelMap model) {
		if (page > 0) {
			model.addAttribute("anterior", page - 1);
		} else {
			page = 0;
		}
		model.addAttribute("pessoas", service.obterTodasPessoas(nome, PageRequest.of(page, size)));
		model.addAttribute("proximo", page + 1);
		return "pessoa/pesquisar";
	}
	
	@GetMapping("/detalhar/{id}")
	public String detalhar(@PathVariable(name = "id") Integer id, ModelMap model) {
		model.addAttribute("pessoa", service.obterPessoa(id));
		return "pessoa/dados";
	}

	
	@GetMapping("/pre-incluir")
	public String preIncluir(ModelMap model) {
		model.addAttribute("pessoa", new Pessoa());
		model.addAttribute("acao", "incluir");
		return "pessoa/dados";
	}

	
	@PostMapping("/incluir")
	public String incluir(@Valid Pessoa pessoa, ModelMap model) {
		model.addAttribute("msg", "Sucesso");
		return pesquisar(null, PAGE_NUMBER_DEFAULT, PAGE_SIZE_DEFAULT, model);
	}

	
	@GetMapping("/pre-alterar/{id}")
	public String preAlterar(@PathVariable(name = "id") Integer id, ModelMap model) {
		model.addAttribute("acao", "alterar");
		return "pessoa/dados";
	}

	
	@PostMapping("/alterar")
	public String alterar(@Valid Pessoa pessoa, ModelMap model) {
		model.addAttribute("msg", "Sucesso");
		return pesquisar(null, PAGE_NUMBER_DEFAULT, PAGE_SIZE_DEFAULT, model);
	}

	
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable(name = "id") Integer id, ModelMap model) {
		return pesquisar(null, PAGE_NUMBER_DEFAULT, PAGE_SIZE_DEFAULT, model);
	}

	
	@GetMapping("/{id}")
	@ResponseBody
	public Pessoa obter(@PathVariable(name = "id") Integer id) {
		return service.obterPessoa(id).get();
	}

}