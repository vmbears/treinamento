package br.com.skalena.treinamento.app;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableWebMvc
@Slf4j
@EnableJpaRepositories(basePackages = "br.com.skalena.treinamento.app.repository")
public class Aplicacao {

	@Autowired
	private PessoaRepository repository;

	public static void main(String[] args) {
        SpringApplication.run(Aplicacao.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void registarDadosIniciais() {
		log.info("registarDadosIniciais - INICIO");
		for (int i = 1; i < 22; i++) {
			repository.save(new Pessoa(i, "exercicio " + i, "lista", LocalDate.now()));
		}
		log.info("registarDadosIniciais - FIM");
	}
}