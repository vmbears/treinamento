package br.com.skalena.treinamento.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	private PessoaRepository repository;

	@Override
	public Optional<Pessoa> obterPessoa(Integer id) {
		return repository.findById(id);
	}

	@Override
	public Page<Pessoa> obterTodasPessoas(String name, Pageable page) {
		Page<Pessoa> pessoas = null;
		if (name == null || name.isEmpty()) {
			pessoas = repository.findAll(page);
		} else {
			pessoas = repository.findByNomeLike(name, page);
		}
		return pessoas;
	}

}