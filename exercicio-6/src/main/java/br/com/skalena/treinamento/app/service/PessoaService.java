package br.com.skalena.treinamento.app.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaService {

	Optional<Pessoa> obterPessoa(Integer id);

	Page<Pessoa> obterTodasPessoas(String name, Pageable page);
	
}
