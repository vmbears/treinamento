# Assunto

Transforme a aplica��o em uma aplica��o web.

# Exercicio

1. No arquivo pom.xml adicione a depend�ncia do spring-boot-starter-web;
2. Crie o arquivo  *PessoaControllerImpl.java*:
	- no pacote *br.com.skalena.treinamento.app.web*;
	- Anote a classe da seguinte forma:
	```
		@Controller
		
		@RequestMapping("/pessoa")
	```
	- Ele deve possuir os m�todos:
	```
		@GetMapping("/{id}")
		@ResponseBody
		public Pessoa obterPessoa(@PathVariable("id") Integer id) {...}

		@GetMapping("/teste/{id}")
		@ResponseBody
		public String obterPessoaAnterior(@PathVariable("id") Integer id, HttpSession session) {...}
	```
	- Consulte a pessoa do Id indicado por meio do *PessoaService*;
	- No m�todo obterPessoaAnterior, concatene ao ToString do objeto pessoa retornado do Id requerido o Id anterior. Para armazenar o id atual utilize o objeto session;

# Dicas

- Segue descri��o da dep�ndencia spring-boot-starter-web:

```
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
```  
- Utilize a Anota��o @Autowired em seu controller para injetar uma inst�ncia de PessoaService;
# Desafio:

N/A
