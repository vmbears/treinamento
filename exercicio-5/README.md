# Assunto

Transformar a aplica��o da primeira parte do treinamento em Spring boot console.
O objetivo ver o quanto o Spring Boot facilita a configura��o de aplica��es, sem mudar o comportamento.
Desta forma fica claro a responsabilidade do componente.  

# Exercicio

1. Na classe Aplicacao.java:
	- adicione a anotação @SpringBootApplication e @EnableJpaRepositories(basePackages = "br.com.skalena.treinamento.app.repository") na classe;
	- Extenda da interface CommandLineRunner e altere o corpo do método main para o método run que será requerido; 
	- No m�todo main inicialize o Spring Boot.
2. Apague a classe Configuracao.java e ConfiguracaoBancoDados.java:
3. Renomeie o arquivo application.properties para application.yml e altere sua estrutura para o mode de hieraquia;
4. Adicionei as configurações de datasource no arquivo application.yml;

# Dicas

- Para iniciliar a Spring Boot chame o contexto da seguinte maneira:

```
	SpringApplication.run(Aplicacao.class, args);
```  
- Para descrever o datasource no application.yml utilize:
```  
spring:
  datasource:
    url: jdbc:h2:mem:testdb
    driverClassName: org.h2.Driver
    username: sa
    password: password
    database-platform: org.hibernate.dialect.H2Dialect 
```
- Altere o parent do pom.xml
```
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.1.5.RELEASE</version>
	</parent>
```
- Remova a depend�ncia do spring-context e adicione
```
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter</artifactId>
	</dependency>
```  

# Desafio:

N/A