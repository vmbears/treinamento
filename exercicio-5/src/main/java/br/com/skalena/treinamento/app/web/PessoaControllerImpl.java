package br.com.skalena.treinamento.app.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.skalena.treinamento.app.entity.Pessoa;
import br.com.skalena.treinamento.app.service.PessoaService;

@Controller
@RequestMapping("/pessoa")
public class PessoaControllerImpl {

	private static final String LAST_ID = "LAST_ID";
	@Autowired
	private PessoaService service;

	@GetMapping("/{id}")
	@ResponseBody
	public Pessoa obterPessoa(@PathVariable("id") Integer id) {
		return service.obterPessoa(id);
	}

	@GetMapping("/teste/{id}")
	@ResponseBody
	public String obterPessoaAnterior(@PathVariable("id") Integer id, @RequestParam(name = "nome", required = false) String nome, HttpSession session) {
		String retorno = session.getAttribute(LAST_ID) + " - " + nome + " - "+ service.obterPessoa(id).toString(); 
		session.setAttribute(LAST_ID, id);
		return retorno;
	}
}