package br.com.skalena.treinamento.app.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application.properties")
@ComponentScan("br.com.skalena.treinamento")
public class Configuracao {

}