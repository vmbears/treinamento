# Assunto

Neste exercicio o objetivo � ver como funcionam a o ciclo de inicializa��o dos beans e a inje��o de dep�ndencias providos pelo Spring Framework. 

# Exercicio

Voc� deve escrever as seguintes classes:

* Pessoa.java: bean com dois atributos id e nome.
* PessoaService.java & PessoaServiceImpl.java: 
	- fazer a cria��o dos objetos pessoas usando como referencia o id passado e o nome padr�o.
	- o nome padr�o deve ser carregado por meio da anota��o @Value("skalena.treinamento.pessoa.nome-padrao")
* Configuracao.java: 
	- ela ser� respons�vel por configurar os beans da aplica��o e carregar o arquivo de configura��o;
	- usar a anota��o @Configuration;
	- usar a anota��o @PropertySource carregando o arquivo "classpath:/application.properties"
* Aplicacao.class: 
	- inicializar o contexto do Spring que ir� gerir os beans;
	- requisitar um bean do tipo PessoaService.class para o Spring;
	- chamar o m�todo obter pessoa que ir� retornar um objeto pessoa preenchido;

# Dicas

* Para criar um contexto do Spring Framework que avalie anota��es para inicializar os beans, utilizar a classe *org.springframework.context.annotation.AnnotationConfigApplicationContext*;
* Para indicar que uma classe ser� de configura��o de beans deve-se utilizar a anota��o @Configuration;
* Para indicar que uma classe seja um bean utilizar anota��o @Component;
* Para carregar um arquivo de configura��o utilize a anota��o @PropertySource. O caminho do arquivo deve ser preenchido com o prefixo "classpath:";
* Para injetar dep�ndencias em suas classes utilize a anota��o @Autowired;

# Desafio:

1. Configurar dois beans do mesmo tipo (PessoaService) no contexto do Spring e fazer escolha do bean a ser utilizado.
	- Veja a anota��o @Primary
2. Configurar o lombok e usar as anota��es como: @Getter, @Setter e @Slf4j, @Data, @NoArgsConstructor e @AllArgsConstructor