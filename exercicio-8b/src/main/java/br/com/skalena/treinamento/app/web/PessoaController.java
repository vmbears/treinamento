package br.com.skalena.treinamento.app.web;

import org.springframework.ui.ModelMap;

import br.com.skalena.treinamento.app.entity.Pessoa;

public interface PessoaController {

	String pesquisar(String nome, ModelMap model);

	Pessoa obter(Integer id);

	String detalhar(Integer id, ModelMap model);

	String preIncluir(ModelMap model);
	
	String incluir(Pessoa pessoa, ModelMap model);

	String preAlterar(Integer id, ModelMap model);

	String alterar(Pessoa pessoa, ModelMap model);

	String excluir(Integer id, ModelMap model);

}
