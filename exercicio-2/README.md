# Assunto

A fim de melhorar o entendimento da utiliza��o do mavem, vamos configur�-lo para gerar um arquivos jar do exerc�cio anterior que seja execut�veis via linha de comando.


# Exerc�cio

* Fa�a a compila��o do projeto via linha de comando utilizando: mvn clean install.
* An�lise o arquivo gerado com o sufixo "jar-with-dependencies.jar"


# Dicas

Copie a confgura��o do plugin assembly para o seu arquivo de build do projeto - pom.xml.

```
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.4.1</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
					<archive>
					  <manifest>
						<mainClass>br.com.skalena.treinamento.app.Aplicacao</mainClass>
					  </manifest>
					</archive>
				</configuration>
				<executions>
				  <execution>
					<id>make-assembly</id>
					<phase>package</phase> 
					<goals>
						<goal>single</goal>
					</goals>
				  </execution>
				</executions>
			</plugin>
```

# Desafio

N/A